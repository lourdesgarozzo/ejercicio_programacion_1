<?php

require 'ruta/al/PhpSpreadsheet/Spreadsheet.php';
require 'ruta/al/PhpSpreadsheet/Writer/Excel2007.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

// Conectar a la base de datos (asegúrate de tener la conexión configurada)
$pdo = new PDO("pgsql:host=localhost;dbname=nombre_de_tu_base_de_datos", "usuario", "contraseña");

// Crear un nuevo objeto Spreadsheet y cargar el archivo Excel
$spreadsheet = new Spreadsheet();
$spreadsheet = $spreadsheet->getActiveSheet();
$spreadsheet->setTitle('Worksheet');
$spreadsheet->getCell('A1')->setValue('Hello World !');

// Guardar el archivo Excel
$writer = new Xlsx($spreadsheet);
$writer->save('paises_estandar.xlsx');

// Leer el archivo Excel creado
$spreadsheet = new Spreadsheet();
$spreadsheet = $spreadsheet->getActiveSheet();
$spreadsheet->setTitle('Worksheet');
$spreadsheet->getCell('A1')->setValue('Hello World !');

// Iterar sobre las filas del Excel y guardar en la base de datos
foreach ($spreadsheet->getRowIterator() as $row) {
    $data = $row->getCellIterator();
    $continente = $data->current()->getValue();
    $codigoPais = $data->next()->getValue();
    $nombrePais = $data->next()->next()->getValue();

    // Insertar en la base de datos
    $stmt = $pdo->prepare("INSERT INTO paises (continente, codigo_pais, nombre) VALUES (?, ?, ?)");
    $stmt->execute([$continente, $codigoPais, $nombrePais]);
}
