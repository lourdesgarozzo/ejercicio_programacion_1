<?php
// Nombre de la base de datos
$dbname = "postgres";

// Nombre de usuario
$user = "postgres";

// Contraseña
$pass = "database";

// Host de la base de datos (puede ser "localhost" si la base de datos está en la misma máquina)
$host = "localhost";

// Número de puerto (el mismo que especificaste en el mapeo de puertos de Docker Compose)
$port = "5432";

try {
    $pdo = new PDO("pgsql:host=postgresql-server;port=$port;dbname=$dbname;user=$user;password=$pass");

    // Configurar el modo de error para que PDO lance excepciones en lugar de emitir advertencias
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    echo "Conexión exitosa a PostgreSQL.";
} catch (PDOException $e) {
    echo "Error al conectar a PostgreSQL: " . $e->getMessage();
}
