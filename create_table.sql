CREATE TABLE paises (
    id SERIAL PRIMARY KEY,
    continente VARCHAR(255) NOT NULL,
    codigo_pais VARCHAR(3) NOT NULL,
    nombre VARCHAR(100) NOT NULL
);