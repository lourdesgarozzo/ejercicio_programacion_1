# Usa la imagen base de PHP con Apache
FROM php:7.4-apache

# Instalar extensiones necesarias
RUN apt-get update && apt-get install -y libpq-dev zlib1g-dev libpng-dev libzip-dev \
    && docker-php-ext-install pdo_pgsql \
    && docker-php-ext-install gd \
    && docker-php-ext-install zip

# Habilitar la extensión pdo_pgsql
RUN docker-php-ext-enable pdo_pgsql

# Instalar Composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

# Establecer el directorio de trabajo
WORKDIR /var/www/html

# Copiar archivos de la aplicación
COPY html/ .

# Instalar dependencias de Composer directamente en el contenedor
RUN composer install --no-dev --optimize-autoloader

# Exponer el puerto 80
EXPOSE 80

# Comando para iniciar el servidor Apache
CMD ["apache2-foreground"]
